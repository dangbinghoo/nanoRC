#
# nanoRC (simple project by dangbinghoo) Makefile
# 
# changelog: 
#            2011.05.26 just added this file.
#            2011.11.17 removed installing runlevels dir
#

TARGET_ROOT ?= dest

all: install

install:
	cp -av src/conf.d ${TARGET_ROOT}/etc
	cp -av src/etc/* ${TARGET_ROOT}/etc
	cp -av src/init.d ${TARGET_ROOT}/etc
	cp -av src/inittab ${TARGET_ROOT}/etc
