nanoRC Documentation
====================================

1. Introduction
-------------------------------------

nanoRC was developed for embedded linux system start-up and system service management. It is suitable for light-weight embedded linux systems, expecially the system which use serial as console for system infomation output. The serail console doesn't have full feature like stardard linux virtual console, different console has different output style, it's very bad if a line text is too long. nanoRC thus has the feature to specify the colum size, and may one long text to be seprated to several lines. and make  the serial console output more clear. beside this, nanoRC has more features like gentoo's openRC, support clear run-levels, gentoo like network configrations and colorized output for system booting and services output message. 

nanoRC is very suitable for light-weight busybox based linux systems and has full feature for system configurations and services management, and make embeeded linux system looks like a more standard linux system.


2. Basic usage and configurations
-------------------------------------

### requirements ###

nanoRC requries busybox and some kernel feature.

busybox need to enable fancy shell ouput and ash with full feature. and the login utils, vi or nano editor, mount utils, and busybox init and support /etc/inittab file.

kernel need to enable devtmpfs and eanble automatical mount of devtmpfs.

### installation ###

to install nanoRC, just type 

    make install 

to specify the installation path , just pass TARGET_ROOT=/your/target/root/path to make. make sure you have permission with your taget path.

### configrations ###

just like gentoo or Arch linux, nanoRC prefer to put system configration file in /etc/conf.d, when you write a personal system service script, you may also put it's configration files in /etc/conf.d.

nanoRC has several stardard config files.

* nanoRC 

edit file in /etc/conf.d/nanoRC.conf, you can specify the colum size and whether to use colorized output.

* hostname

edit file /etc/conf.d/hostname to change your hostname.

* network

edit file /etc/conf.d/net to change your network configration. nanoRC network work service support multi network interfaces. to specify t he network params with interface eth0, you may do like this:

    # DHCP
    # eth0_link="dhcp"
    
    # static 
    eth0_link="static" 
    eth0_ip="192.168.42.146"
    eth0_mac="00:22:3f:44:55:66"
    eth0_netmask="255.255.255.0"
    eth0_gateway="192.168.42.1"

    # nameserver
    nameserver="61.139.2.69"

ethx_link can be configured as dhcp or static, if you use static, you may specify ip, clone mac, netmask and gateway. if you need one more interface net service script, just link /etc/init.d/network to network.ethx the x is number of inteface name surffix.

* system services or runlevels

the file in /etc/conf.d/runlevels.conf tell nanoRC howto boot system and start or stop service. 

    # services for default run level.
     level_default="automodev network.eth0 telnetd"

    # services need to stopped when system rebooting.
     level_reboot="telnetd"

    # services need to stopped when system shutting down.
     level_shutdown="telnetd"

for KISS, nanoRC just support one two runlevels, one is boot and one is default. the boot runlevel script just remount root filesystem as read-write, and mount other filesystems like proc, sys, and setting hostname. the default runlevel is the runlevel for user, here we start the automodev and network and telnetd services. automodev service will automatically insmod modules and create device nodes in /dev, thus, your app or the other services can running in a prepared env.

* other config file in /etc

many other services or apps may use config files in /etc rather than /etc/conf.d, for example, the vsftpd or snmpd or boa. for these apps, you may directly edit the config files and it will not has any relation-ships with nanoRC except the service scripts.


3. the Core shell lib of nanoRC
-------------------------------------

nanoRC is formed with some configration files and services scripts. service script will be a shell script, and in nanoRC, it's a ash script. ash is different with bash, some features was lacked, we may use only POSIX shell scripting skills.

nanoRC supplied a shell lib for writting service script, which located in /etc/init.d, the rcfuntions file. rcfuntions supplied several standard function for start or stop a deamon or an app. and to output hint info when doing service stop or start. rcfuntion makes these feature avaliable for users:

* rc messages 

rc_do_msg, rc_warning, rc_error, rc_notify, rc_print_status,  the rc_do_msg notify the user some service is starting, and rc_warning/error/notify is for output hint info about something, these message output method will output message with a new line. rc_print_status is used to output the status of starting or stopping a daemon.

* daemon or service management

stop_daemon start_daemon autorestart_daemon.

you may use start/stop_daemon for the general sevices. autorestart_daemon will start a service but take the daemon, meanning that if service app was exited or crashed, the nanoRc will restart service again, this makes your service more reliable.


4. How tho write a system service script
----------------------------------------

here we take a vsftpd service files for example:


    #!/bin/sh
    #
    # rc Script for vsftpd.
    # (c) dangbinghoo@gmail.com.
    #     playpenguin.org
    #
    
    cmd_bin="/usr/sbin/vsftpd"
    cmd_param="/etc/vsftpd.conf"
    
    . /etc/init.d/rcfunctions
    
    start()
    {   
        rc_do_msg "Starting vsftpd "
        start_daemon "${cmd_bin}" "${cmd_param}" "0" "${cmd_bin}" 
        rc_print_status
    }
    
    status()
    {
        pid=`rc_check_proc_cmd "${cmd_bin}"`
    
        if [ -n "${pid}" ];then
            rc_warning "service already started with pid "${pid}"."
        else
            rc_warning "service not started!"
        fi
    }

    restart()
    {
        stop
        start
    }
    
    stop()
    {
        rc_do_msg "Stopping telnetd "
        stop_daemon "${cmd_bin}"
        rc_print_status
    }
    
    #
    # main
    #

    case "$1" in
        "start")
            start;;
        "status")
            status;;
        "stop") 
            stop;;
        "restart")
            restart;;
        *)
            echo "Usage: $0 start|stop|restart"
            exit 0
    esac

start a servie with start_daemon fuction, and $3 param is the time to delay, the service will start after $3 seconds. for a better and fancy output of system booting, you may always use rc_do_msg to notify a service startup, and rc_print_status for the status of service stating.

rc_check_proc_cmd can get a pid of command or app, helps you to check whether the app started properly.


5. other
----------------------------------------

for a more featured embeede linux, you may eanble more utils in busybox.
